" Save time in C in Vim.
" Maintainer:	Segmentation Fault <segv@disroot.org>
" Last Change:  2021-10-20
" Version: 1.4
" Repository: https://codeberg.org/segv/cmap.vim
" License: GPLv3

if !exists('g:cmap_typemaps')
	let g:cmap_typemaps = {
		\ "void":	"v",
		\ "char":	"c",
		\ "short":	"s",
		\ "int":	"i",
		\ "long":	"l",
		\ "long long":	"L",
		\ "float":	"f",
		\ "double":	"F",
		\ }
endif


func! s:cmap_functypemap(rettype, short)
	let map_mid = "()<cr>{<cr><cr>}<cr><esc>?"
	let map_end = "<cr>f(i"

	execute "inoremap <buffer> ;f"  .a:short." "               .a:rettype." " .map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;fu" .a:short." unsigned "      .a:rettype." " .map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;fp" .a:short." "               .a:rettype." *".map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;fpu".a:short." unsigned "      .a:rettype." *".map_mid.a:rettype.map_end

	execute "inoremap <buffer> ;F"  .a:short." const "         .a:rettype." " .map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;Fu" .a:short." const unsigned ".a:rettype." " .map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;Fp" .a:short." const "         .a:rettype." *".map_mid.a:rettype.map_end
	execute "inoremap <buffer> ;Fpu".a:short." const unsigned ".a:rettype." *".map_mid.a:rettype.map_end
endf

func! s:cmap_castmap(type, short)
	execute "inoremap <buffer> ;c"  .a:short." ("               .a:type.")"
	execute "inoremap <buffer> ;cu" .a:short." (unsigned "      .a:type.")"
	execute "inoremap <buffer> ;cp" .a:short." ("               .a:type." *)"
	execute "inoremap <buffer> ;cpu".a:short." (unsigned "      .a:type." *)"

	execute "inoremap <buffer> ;C"  .a:short." (const "         .a:type.")"
	execute "inoremap <buffer> ;Cu" .a:short." (const unsigned ".a:type.")"
	execute "inoremap <buffer> ;Cp" .a:short." (const "         .a:type." *)"
	execute "inoremap <buffer> ;Cpu".a:short." (const unsigned ".a:type." *)"
endf


func! s:cmap_varmap(vtype, short)
	let map_end = ";<left>"

	execute "inoremap <buffer> ;v"  .a:short." "               .a:vtype." " .map_end
	execute "inoremap <buffer> ;vu" .a:short." unsigned "      .a:vtype." " .map_end
	execute "inoremap <buffer> ;vp" .a:short." "               .a:vtype." *".map_end
	execute "inoremap <buffer> ;vpu".a:short." unsigned "      .a:vtype." *".map_end

	execute "inoremap <buffer> ;V"  .a:short." const "         .a:vtype." " .map_end
	execute "inoremap <buffer> ;Vu" .a:short." const unsigned ".a:vtype." " .map_end
	execute "inoremap <buffer> ;Vp" .a:short." const "         .a:vtype." *".map_end
	execute "inoremap <buffer> ;Vpu".a:short." const unsigned ".a:vtype." *".map_end
endf

func! s:cmap_funcmap(name, short)
	let map_end = "();<esc>0f)i"
	execute "inoremap <buffer> ;c"  .a:short." ("         .a:name.") "  .map_end
	execute "inoremap <buffer> ;cu" .a:short." (unsigned ".a:name.") "  .map_end
	execute "inoremap <buffer> ;cp" .a:short." ("         .a:name." *) ".map_end
	execute "inoremap <buffer> ;cpu".a:short." (unsigned" .a:name." *) ".map_end

	execute "inoremap <buffer> ;C"  .a:short." (const "         .a:name.") "  .map_end
	execute "inoremap <buffer> ;Cu" .a:short." (const unsigned ".a:name.") "  .map_end
	execute "inoremap <buffer> ;Cp" .a:short." (const "         .a:name." *) ".map_end
	execute "inoremap <buffer> ;Cpu".a:short." (const unsigned" .a:name." *) ".map_end
endf

augroup c_maps
	autocmd!
	" preprocessing
	autocmd filetype c,cpp  inoremap <buffer> ;pi #include <><Left>
	autocmd filetype c,cpp  inoremap <buffer> ;pI #include ""<Left>
	autocmd filetype c,cpp  inoremap <buffer> ;pd #define<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pD #undef<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pn #ifdef<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pN #ifndef<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pf #if<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pF #elif<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pe #else<space>
	autocmd filetype c,cpp  inoremap <buffer> ;pE #endif<space>

	" main
	autocmd filetype c,cpp  inoremap <buffer> ;fm int main(int argc, const char *argv[])<cr>{<cr><cr><home>}<up><tab>
	autocmd filetype c,cpp  inoremap <buffer> ;fM int main(int argc, const char **argv)<cr>{<cr><cr><home>}<up><tab>

	" type
	autocmd filetype c,cpp  for [type, short] in items(g:cmap_typemaps)
		\| call s:cmap_functypemap(type, short)
		\| call s:cmap_varmap(type, short)
		\| call s:cmap_castmap(type, short)
	\| endfor

	" standard function
	autocmd filetype c,cpp  inoremap <buffer> ;sp printf("");<esc>0ci"
	autocmd filetype c,cpp  inoremap <buffer> ;sP fprintf(stderr, "");<esc>0ci"

	" loop
	autocmd filetype c,cpp  inoremap <buffer> ;lg for(int i = 0; i > <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;lG for(int i = 0; i => <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;ll for(int i = 0; i < <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;lL for(int i = 0; i =< <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;le for(int i = 0; i = <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;lE for(int i = 0; i != <>; i++) {<cr><cr>}<esc>?<><cr>xxi
	autocmd filetype c,cpp  inoremap <buffer> ;L while(<>) {<cr><cr>}<esc>?<><cr>xxi

augroup end
