# cmap.vim
Save time in C in Vim.

## Configuration
Set the `g:cmap_typemaps` dictionary.

For example, the following
```
let g:cmap_typemaps = {
	\ "int":	"i",
	\ }
```
will have the following map
```
;[v|V|f|F|c|F][p][u]i
```

* `v` create a variable
* Replace `v` with `V` make the variable `const`
* `c` create a cast
* Replace `c` with `C` make the cast `const`
* `f` create a function
* Replace `f` with `F` make the function return type `const`
* `p` make it a pointer
* `u` make it unsigned
